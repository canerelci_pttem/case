<?php

namespace Mrtyz\Pttamv\Tests\Google;

use Mrtyz\Pttamv\Tests\BaseTestCase;

class ProductServiceTest extends BaseTestCase
{
    public function testCreateProducts()
    {
        $result = $this->cimri->productService()
            ->creatingProducts()
            ->addProduct($this->getTestProduct())
            ->createToJson();

        $this->assertEquals(201, $result["statusCode"]);
    }
    
    /**
     *   Put Patch Delete ETC.
     */
}