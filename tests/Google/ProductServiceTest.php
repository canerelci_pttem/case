<?php

namespace Google;

use Mrtyz\Pttamv\Tests\BaseTestCase;

class ProductServiceTest extends BaseTestCase
{
    public function testCreateProducts()
    {
        $result = $this->google->productService()
            ->creatingProducts()
            ->addProduct($this->getTestProduct())
            ->create();
        
        $this->assertEquals(201, $result["statusCode"]);
    }
    
    /**
     *   Update Delete ETC.
     */
}