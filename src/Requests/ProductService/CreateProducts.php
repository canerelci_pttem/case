<?php

namespace Mrtyz\Pttavm\Requests\ProductService;

use Mrtyz\Pttavm\Abstracts\AbstractRequest;
use Mrtyz\Pttavm\Interfaces\IRequest;

class CreateProducts extends AbstractRequest implements IRequest
{

    public function getMethod(): string
    {
        return self::METHOD_POST;
    }
}