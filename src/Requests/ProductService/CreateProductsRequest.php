<?php
namespace Mrtyz\Pttavm\Requests\ProductService;

use Mrtyz\Pttavm\Abstracts\AbstractRequestBuilder;
use Mrtyz\Pttavm\Helpers\RequestManager;
use Mrtyz\Pttavm\Interfaces\IRequestBuilder;
use Mrtyz\Pttavm\Product\Product;

class CreateProductsRequest extends AbstractRequestBuilder implements IRequestBuilder
{
    public function __construct(RequestManager $requestManager)
    {
        parent::__construct($requestManager);

        $this->setRequest(CreateProducts::create());
    }

    public function addProduct(Product $product): self
    {
        $this->request->addData('product', $product->toArray(), true);

        return $this;
    }

    public function createToJson()
    {
        return $this->processByJson();
    }

    public function createToXml()
    {
        return $this->processByXml();
    }
}
