<?php

namespace Mrtyz\Pttavm\Services;

use Mrtyz\Pttavm\Abstracts\AbstractService;
use Mrtyz\Pttavm\Interfaces\IService;
use Mrtyz\Pttavm\Requests\ProductService\CreateProductsRequest;

class ProductService extends AbstractService implements IService
{
    /**
     * @return CreateProductsRequest
     */
    public function creatingProducts(): CreateProductsRequest
    {
        return new CreateProductsRequest($this->requestManager);
    }
}