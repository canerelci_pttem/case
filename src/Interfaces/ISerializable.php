<?php

namespace Mrtyz\Pttavm\Interfaces;

use JsonSerializable;

interface ISerializable extends JsonSerializable
{
    public function toJson();
}