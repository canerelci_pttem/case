<?php

namespace Mrtyz\Pttavm\Interfaces;

interface IRequest
{
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';
    const METHOD_PUT = 'put';
    const METHOD_DELETE = 'delete';

    public function __construct(array $data = []);

    public function getMethod(): string;

    public function getPath(): string;

    public function getData(string $key = '');

    public function setData(array $data): self;
}