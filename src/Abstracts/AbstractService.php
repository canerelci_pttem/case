<?php

namespace Mrtyz\Pttavm\Abstracts;

use Mrtyz\Pttavm\Helpers\RequestManager;

abstract class AbstractService
{
    protected RequestManager $requestManager;
    
    public function __construct(RequestManager $requestManager)
    {
        $this->requestManager = $requestManager;
    }
}