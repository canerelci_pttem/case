<?php

namespace Mrtyz\Pttavm\Abstracts;

use Mrtyz\Pttavm\Interfaces\IRequest;

abstract class AbstractRequest implements IRequest
{
    private array $data = [];

    /**
     * AbstractRequest constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @param array $data
     *
     * @return static
     */
    public static function create(array $data = []): self
    {
        return new static($data);
    }

    /**
     * Returns the path after then replace the variables.
     *
     * @return string
     */
    public function getPath(): string
    {
        $path = $this->getPathPattern();

        return $path;
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getData(string $key = '')
    {
        if (empty($key)) {
            return $this->data;
        }

        return $this->data[$key] ?? null;
    }
    

    /**
     * @param string $key
     * @param $value
     * @param bool $isNew
     *
     * @return $this
     */
    public function addData(string $key, $value, bool $isNew = false): self
    {
        if (!$isNew) {
            $this->data[$key] = $value;
        } else {
            $this->data[$key][] = $value;
        }

        return $this;
    }
}
