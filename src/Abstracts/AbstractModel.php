<?php
namespace Mrtyz\Pttavm\Abstracts;

use Mrtyz\Pttavm\Interfaces\IModel;
use Mrtyz\Pttavm\Traits\TJsonable;

abstract class AbstractModel implements IModel
{
    use TJsonable;
    
    public function __construct()
    {
    }
}
