<?php

namespace Mrtyz\Pttavm\Abstracts;

use Mrtyz\Pttavm\Interfaces\IRequest;
use Mrtyz\Pttavm\Helpers\RequestManager;

abstract class AbstractRequestBuilder
{
    protected IRequest $request;

    protected RequestManager $requestManager;

    /**
     * AbstractRequestBuilder constructor.
     *
     * @param RequestManager $requestManager
     */
    public function __construct(RequestManager $requestManager)
    {
        $this->requestManager = $requestManager;
    }

    /**
     * @param IRequest $request
     *
     * @return $this
     */
    public function setRequest(IRequest $request): self
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return IRequest
     */
    public function getRequest(): IRequest
    {
        return $this->request;
    }

    /**
     * @return mixed
     */
    protected function processByJson()
    {
        return $this->requestManager->processByJson($this->request);
    }

    /**
     * @return mixed
     */
    protected function processByXml()
    {
        return $this->requestManager->processByXml($this->request);
    }
}
