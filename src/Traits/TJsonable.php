<?php
namespace Mrtyz\Pttavm\Traits;

trait TJsonable
{
    public function toJson()
    {
        return json_encode($this->jsonSerialize());
    }
}
