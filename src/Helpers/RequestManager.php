<?php
namespace Mrtyz\Pttavm\Helpers;

use Mrtyz\Pttavm\Helpers\Client;
use GuzzleHttp\RequestOptions;
use Mrtyz\Pttavm\Interfaces\IRequest;

class RequestManager
{
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function processByJson(IRequest $IRequest)
    {
        $method = $IRequest->getMethod();
        $path = $IRequest->getPath();
        $data = $IRequest->getData();

        $response = $this->client->$method($path, [
            RequestOptions::SYNCHRONOUS => false,
            RequestOptions::HEADERS     => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
            ],
            RequestOptions::JSON      => $data,
        ]);

        return json_decode($response->getBody());
    }
    
    public function processByXml(IRequest $IRequest)
    {
        // TODO : kod tekrari ve xml converter yapilacak
        $method = $IRequest->getMethod();
        $path = $IRequest->getPath();
        $data = $IRequest->getData();

        $response = $this->client->$method($path, [
            RequestOptions::SYNCHRONOUS => false,
            RequestOptions::HEADERS     => [
                'Accept'       => 'application/xml',
                'Content-Type' => 'text/xml',
            ],
            $data,
        ]);

        return json_decode($response->getBody());
    }

    public function getClient(): Client
    {
        return $this->client;
    }
}
