<?php

namespace Mrtyz\Pttavm\Helpers;

use GuzzleHttp\Client as HttpClient;

class Client extends HttpClient
{
    private string $base_uri;

    private string $user;

    private string $pass;

    private string $merchant_id;

    public function __construct(string $user, string $pass, string $merchant_id, $base_uri)
    {
        $this->base_uri = $base_uri;
        $this->user = $user;
        $this->pass = $pass;
        $this->merchant_id = $merchant_id;

        parent::__construct([
            'base_uri' =>  $this->base_uri,
            'auth'     => [$this->user, $this->pass],
        ]);
    }

    /**
     * @return string
     */
    public function getMerchantId(): string
    {
        return $this->merchant_id;
    }
    

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }

    public function getBaseUri(): string
    {
        return $this->base_uri;
    }
}