<?php
namespace Mrtyz\Pttavm\Product;

use Mrtyz\Pttavm\Abstracts\AbstractModel;

class Product extends AbstractModel
{
    public string $title;

    public string $description;

    public int $quantity;

    public string $currencyType;

    public float $salePrice;

    public function jsonSerialize()
    {
        return $this->toArray();
    }
    
    public function toArray(): array
    {
        return [
            'title'        => $this->title,
            'description'  => $this->description,
            'quantity'     => $this->quantity,
            'currencyType' => $this->currencyType,
            'salePrice'    => $this->salePrice,
        ];
    }
}
