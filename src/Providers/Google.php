<?php

namespace Mrtyz\Pttavm\Provider;

use Mrtyz\Pttavm\Services\ProductService;
use Mrtyz\Pttavm\Helpers\Client;
use Mrtyz\Pttavm\Helpers\RequestManager;

class Google
{
    private RequestManager $requestManager;
    
    public function __construct(string $user, string $pass, string $merchant_id, string $base_uri)
    {
        $client = new Client($user, $pass, $merchant_id, $base_uri);
        $this->requestManager = new RequestManager($client);
    }

    public function productService(): ProductService
    {
        return new ProductService($this->requestManager);
    }
}
